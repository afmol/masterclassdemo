
Gebruik Generators om snel modellen, Controllers, Migration, Seed en Views te maken

In Compser

    "way/generators": "2.*"

composer self-update
composer update --dev





ssh
sudo ln -s /opt/VBoxGuestAdditions-4.3.10/lib/VBoxGuestAdditions /usr/lib/VBoxGuestAdditions

vagrant ssh
# then, inside the VM:
sudo ln -s /opt/VBoxGuestAdditions-4.3.10/lib/VBoxGuestAdditions /usr/lib/VBoxGuestAdditions
logout
# then, outside the VM:
vagrant reload

sudo /etc/init.d/nginx stop
sudo /etc/init.d/apache2 restart
sudo netstat -ltnp | grep ':80'

sudo ln -s /home/vagrant/apps/default/public /var/www
https://www.digitalocean.com/community/articles/how-to-set-up-apache-virtual-hosts-on-debian-7