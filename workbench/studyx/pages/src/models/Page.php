<?php namespace Studyx\Pages;

use Illuminate\View\Environment;
use Illuminate\Config\Repository;

class Page extends \Eloquent {

	// Add your validation rules here
	public static $rules = [
		'title' => 'required'
	];

	// Don't forget to fill this array
	protected $fillable = [];

    /**
     * Illuminate view environment.
     *
     * @var Environment
     */
    protected $view;

    /**
     * Illuminate config repository.
     *
     * @var Repository
     */
    protected $config;

    /**
     * Create a Page instance
     *
     * @param Environment $view
     * @param Repository  $config
     * @return void
     */
    public function __construct(Environment $view, Repository $config) {
        $this->view = $view;
        $this->config = $config;
    }

    public function testMe() {
        return "Yes, you have included me";
    }

    /**
     * Generate and return a report.
     *
     * @return View
     */
    public function generateReport()
    {
        $count = rand(4, 40);
        $state = $this->config->get('pages::enabled');

        $lines = array();
        $lines[] = array("title" => "Report Info", "info" => "Report created on " . time());
        $lines[] = array("title" => "Status", "info" => $state);
        $lines[] = array("title" => "Page count", "info" => "Total pages: " . $count);
        $lines[] = array("title" => "Final", "info" => "Report ready");

        return $this->view->make('pages::report', compact('count', 'lines'));
    }

    public function toggle() {
        $state = $this->config->get('page::enabled', false);
        $this->config->set('page::enabled', !$state);
        echo "State toggled:" . $this->config->get('page::enabled') . '<br>';
    }

    /**
     * Enable the page info.
     *
     * @return void
     */
    public function enable()
    {
        $this->config->set('page::enabled', true);
        echo "State:" . $this->config->get('page::enabled');
    }

    /**
     * Disable the page info.
     *
     * @return void
     */
    public function disable()
    {
        $this->config->set('page::enabled', false);
        echo "State:" . $this->config->get('page::enabled');
    }
}