@extends('layouts.bootstrap')

@section('content')

@include('layouts.navbar', array('resource' => $resource))
<h1>Showing {{ $item }}</h1>

<div class="jumbotron text-center">
    <h2>{{ $item }}</h2>
    <p>
        <strong>Title:</strong> {{ $item->title }}<br>
        <strong>Content:</strong> {{ $item->content }}<br>
        <strong>Active:</strong> {{ $item->active }}<br>
        <strong>Published:</strong> {{ $item->published }}<br>
    </p>
</div>
@stop