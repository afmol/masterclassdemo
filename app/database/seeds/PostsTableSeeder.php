<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class PostsTableSeeder extends Seeder {

	public function run()
	{
        $users = User::all();

		$faker = Faker::create();

        foreach($users as $user) {
            foreach(range(1, 10) as $index)
            {
                Post::create([
                    'title' => $faker->sentence(5),
                    'content' => $faker->paragraph,
                    'user_id' => $user->id
                ]);
            }
        }
	}

}