<?php

class Comment extends \Eloquent {

	// Add your validation rules here
	public static $rules = [
	    'content' => 'required'
	];

	// Don't forget to fill this array
	protected $fillable = [
        'content',
        'active'
    ];

    public function post()
    {
        return $this->belongsTo('Post');
    }

    public function user()
    {
        return User::find($this->user_id);
    }
}