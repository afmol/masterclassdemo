<?php
/**
 * Created by PhpStorm.
 * User: Harry van der Valk @ HVSoftware
 * Date: 2-5-14
 * Time: 22:13
 */

class AppFactory
{
    /**
     * @param User $user
     * @param $name
     * @param $email
     * @return Author
     */
    public static function createAuthor(User $user, $name, $email)
    {
        $author = new Author($user);
        $author->setName($name);
        $author->setEmail($email);
        return $author;
    }

    /**
     * @param $title
     * @param $content
     * @param Category $category
     * @return Post
     */
    public static function createPost($title, $content, Category $category = null)
    {
        $post = new Post();
        $post->title = $title;
        $post->content = $content;
        if ($category) {
            $post->addCategory($category);
        }
        $author = Author::find(1);
        $post->author_id = $author->id;
        return $post;
    }

    /**
     * @param Post $post
     * @param $action
     * @return PostEvent
     */
    public static function createPostEvent(Post $post, $action)
    {
        $event = new PostEvent();
        $event->post_id = $post->id;
        $event->action = $action;
        return $event;
    }

    /**
     * @param $content
     * @param Post $post
     * @return Comment
     */
    public static function createComment($content, Post $post = null)
    {
        $comment = new Comment();
        $comment->setContent($content);
        $comment->setPost($post);
        return $comment;
    }

    /**
     * @param $title
     * @param bool $active
     * @param Post $post
     * @return Category
     */
    public static function createCategory($title, $active = true, Post $post = null) {
        $category = new Category();
        $category->setTitle($title);
        $category->setActive($active);
        $category->setPost($post);
        return $category;
    }
} 