<?php
/**
 * Created by PhpStorm.
 * User: Harry van der Valk @ HVSoftware
 * Date: 20-5-14
 * Time: 13:00
 */

/**
 * Class PostEventType
 */
class PostEventType {

    const UNKNOWN = 0;
    const NEWLY = 1;
    const SAVED = 5;
    const DELETED = 10;
} 