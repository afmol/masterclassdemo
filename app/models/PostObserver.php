<?php
/**
 * Created by PhpStorm.
 * User: Harry van der Valk @ HVSoftware
 * Date: 19-5-14
 * Time: 22:44
 */

/**
 * creating, created, updating, updated, saving, saved, deleting,
 * deleted, restoring, restored
 * Class PostObserver
 */
class PostObserver {

    /**
     * @param Post $model
     */
    public function created(Post $model) {
        //$model->content = '[CREATED]' . $model->content;
    }

    /**
     * @param Post $model
     */
    public function saving(Post $model)
    {
        //
    }

    /**
     * @param Post $model
     */
    public function saved(Post $model)
    {
//        echo "Post is created.";
//        die;
        //
        $event = new PostEvent();
        $event->post_id = $model->id;
        $event->action = 'SAVED';
        $event->type = PostEventType::SAVED;
        $event->save();
    }

    /**
     * @param Post $model
     */
    public function deleted(Post $model) {
        $event = new PostEvent();
        $event->post_id = $model->id;
        $event->action = 'DELETED';
        $event->type = PostEventType::DELETED;
        $event->save();
    }

}