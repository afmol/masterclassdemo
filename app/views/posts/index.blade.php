@extends('layouts.bootstrap')

@section('content')

<h1>All posts </h1>

<a href="{{ URL::to($resource .'/create')}}">Create a {{ucfirst($resource) }}</a>

<table class="table table-striped table-bordered">
    <thead>
    <tr>
        <td>ID</td>
        <td>Title</td>
        <td>Authour</td>
        <td>Actions</td>
    </tr>
    </thead>
    <tbody>
    @foreach($list as $key => $value)
    <tr>
        <td>{{ $value->id }}</td>
        <td>{{ $value->title }}</td>
        <td>{{ $value->author_id }}</td>

        <!-- we will also add show, edit, and delete buttons -->
        <td>

            <!-- delete the nerd (uses the destroy method DESTROY /nerds/{id} -->
            <!-- we will add this later since its a little more complicated than the other two buttons -->

            <!-- show the nerd (uses the show method found at GET /nerds/{id} -->
            <a class="btn btn-small btn-success" href="{{ URL::to( $resource . '/' . $value->id) }}">Show</a>

            <!-- edit this nerd (uses the edit method found at GET /nerds/{id}/edit -->
            <a class="btn btn-small btn-info" href="{{ URL::to( $resource . '/' . $value->id . '/edit') }}">Edit</a>

        </td>
    </tr>
    @endforeach
    </tbody>
</table>
@stop

