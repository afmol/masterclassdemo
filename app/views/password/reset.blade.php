@extends('layouts.bootstrap')

@section('title')
{{ Lang::get('password.reset') }}
@stop

@section('content')

<h1>{{ Lang::get('password.reset') }}</h1>
<form action="{{ action('RemindersController@postReset') }}" method="POST">
    <input type="hidden" name="token" value="{{ $token }}">
    <input type="email" name="email">
    <input type="password" name="password">
    <input type="password" name="password_confirmation">
    <input type="submit" value="Reset Password">
</form>

@stop