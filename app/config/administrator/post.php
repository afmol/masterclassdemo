<?php

/**
 * Actors model config
 */

return array(

    'title' => 'Posts',

    'single' => 'post',

    'model' => 'Post',

    /**
     * The display columns
     */
    'columns' => array(
        'id',
        'title' => array(
            'title' => 'Title',
            'select' => "(:table).title",
        ),
    ),

    /**
     * The filter set
     */
    'filters' => array(
        'id',
        'title' => array(
            'title' => 'Title',
        ),
    ),

    /**
     * The editable fields
     */
    'edit_fields' => array(
        'title' => array(
            'title' => 'Title',
            'type' => 'text',
        ),
    ),

);