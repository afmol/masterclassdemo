<?php

/**
 * Actors model config
 */

return array(

    'title' => 'Comments',

    'single' => 'comment',

    'model' => 'Comment',

    /**
     * The display columns
     */
    'columns' => array(
        'id',
        'content' => array(
            'title' => 'Content',
            'select' => "(:table).content",
        ),
        'active' => array(
            'title' => 'Active',
            'select' => "(:table).active",
        ),
    ),

    /**
     * The filter set
     */
    'filters' => array(
        'content' => array(
            'title' => 'Content',
        ),
        'active' => array(
            'title' => 'Active',
        ),
    ),

    /**
     * The editable fields
     */
    'edit_fields' => array(
        'content' => array(
            'title' => 'Content',
            'type' => 'text',
        ),
        'active' => array(
            'title' => 'Active',
            'type' => 'bool',
        ),
    ),

);