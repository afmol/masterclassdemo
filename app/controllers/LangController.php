<?php
/**
 * Created by PhpStorm.
 * User: Harry van der Valk @ HVSoftware
 * Date: 5-5-14
 * Time: 22:51
 */

class LangController {

    public function change($lang) {
        App::setLocale($lang);
        return Redirect::to('home');
    }
} 